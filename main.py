import argparse
import json

from tts_gen import config
from tts_gen import engine


def get_args():
    parser = argparse.ArgumentParser(description="TTS input data generator")
    parser.add_argument("--locations", type=int,
                        help="Number of locations "
                             "(default: %(default)s)", default=10)
    parser.add_argument("--entities", type=int,
                        help="Number of entities "
                             "(default: %(default)s)", default=10)
    parser.add_argument("--events", type=int,
                        help="Number of events "
                             "(default: %(default)s)", default=20)
    parser.add_argument("--time-slots", type=int,
                        help="Number of time slots in time container "
                             "(default: %(default)s)",
                        default=6)

    return parser.parse_args()


def main():

    args = get_args()
    conf = config.Config("input.yaml")
    result = engine.get_result(locations=args.locations,
                               entities=args.entities,
                               events=args.events, cfg=conf,
                               ts_in_tc=args.time_slots)
    with open('output.json', 'w') as fp:
        json.dump(result, fp)


if __name__ == "__main__":
    main()
