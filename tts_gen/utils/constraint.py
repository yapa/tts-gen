import random


class ConstraintBuilder(object):

    def __init__(self):
        self.required_features = {
                "entity": [],
                "location": []}

    def create_constraints(self, element_type, element_name):

        generators = {
            "entities": lambda: self._get_entity_constraints(),
            "events": lambda: self._get_event_constraints(element_name)
        }

        gen_fn = generators.get(element_type, lambda: None)
        return gen_fn()

    def get_required_features(self):
        return self.required_features

    def _get_event_constraints(self, element_name):
        spec = element_name + " teacher"
        capacity = random.randint(5, 20)
        self.required_features["entity"].append(("specialization", spec))
        self.required_features["location"].append(("capacity", capacity))

        return [{"type": "FeatureValueConstraint",
                 "weight": random.randint(1, 10),
                 "target": "location",
                 "expression": {
                      "feature_name": "capacity",
                      "op": "gt",
                      "value": capacity}},
                {"type": "FeatureValueConstraint",
                 "weight": random.randint(1, 10),
                 "target": "entity",
                 "expression":
                     {"feature_name": "specialization",
                      "op": "eq",
                      "value": spec}}]

    def _get_entity_constraints(self):
        return [{"type": "FeatureValueConstraint",
                 "weight": random.randint(1, 10),
                 "target": "location",
                 "expression": {
                      "feature_name": "disable_friendly",
                      "op": "eq",
                      "value": random.choice([True, False])}}]
