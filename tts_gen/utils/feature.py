import random


class FeatureBuilder(object):

    def __init__(self, required_features):
        self.required_features = required_features
        self.fullfilled_req = {k: [] for k in required_features.keys()}

    def create_features(self, element_type, element_name):
        generators = {
            "locations": lambda: self._get_location_features(),
            "entities": lambda: self._get_entity_features(),
        }
        gen_fn = generators.get(element_type, lambda: None)
        return gen_fn()

    def _get_location_features(self):
        return [{"name": "capacity",
                 "value": self._get_feature_value("location", "capacity")},
                {"name": "disable_friendly",
                 "value": random.choice([True, False])}]

    def _get_entity_features(self):
        return [{"name": "specialization",
                 "value": self._get_feature_value("entity", "specialization")}]

    def _get_feature_value(self, element_type, feature_name):
        required_features = [x for x in self.required_features[element_type]
                             if x[0] == feature_name]
        fullfilled_req = [x for x in self.fullfilled_req[element_type]
                          if x[0] == feature_name]

        choices = set(required_features) - set(fullfilled_req)
        if len(choices) != 0:
            result = choices.pop()
            fullfilled_req.append(result)
        else:
            result = random.choice(required_features)
        return result[1]
