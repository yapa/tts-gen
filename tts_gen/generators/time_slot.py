from tts_gen.generators import base


class TimeSlotGenerator(base.BaseGenerator):

    def __init__(self, config_object, maximum):
        super(TimeSlotGenerator, self).__init__(config_object, maximum)
        self.element.update({
            "time_container": None,
            "previous": None,
            "next": None
        })
        self.previous = None
        self.groups = self.config.ts_groups[:]
        self.ts_in_tc = maximum / len(self.config.ts_groups)

    def __next__(self):
        if self.x >= self.max:
            raise StopIteration
        self.element["id"] = self.x
        if self.x % self.ts_in_tc == 0:
            self.element["previous"] = None
            self.element["next"] = self.x + 1
            self.group = self.groups.pop(0)
        elif self.x % self.ts_in_tc == self.ts_in_tc - 1:
            self.element["previous"] = self.x - 1
            self.element["next"] = None
        else:
            self.element["previous"] = self.x - 1
            self.element["next"] = self.x + 1

        self.element["time_container"] = self.group
        self.x += 1

        return self.element.copy()
