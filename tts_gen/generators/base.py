class BaseGenerator(object):

    def __init__(self, config_object, maximum):
        self.element = {
            "id": -1,
            "name": None,
            "features": None,
            "constraints": None
        }
        self.config = config_object
        self.max = maximum
        self.x = 0

    def _get_name(self):
        pass

    def _get_id(self):
        pass

    def __iter__(self):
        return self

    def __next__(self):
        if self.x >= self.max:
            raise StopIteration
        self.element["id"] = self._get_id()
        self.element["name"] = self._get_name()
        self.x += 1
        return self.element.copy()
