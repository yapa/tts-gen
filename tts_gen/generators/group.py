from tts_gen.generators import base


class GroupGenerator(base.BaseGenerator):

    def __init__(self, config_object, names):
        super(GroupGenerator, self).__init__(config_object, len(names))
        self.names = names

    def _get_name(self):
        if self.names:
            return self.names[self.x]
        raise StopIteration

    def _get_id(self):
        group_id = self.config.get_global_variable("group_id") + 1
        self.config.set_global_variable("group_id", group_id)
        return group_id
