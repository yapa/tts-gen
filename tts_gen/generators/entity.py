import faker

from tts_gen.generators import base


class EntityGenerator(base.BaseGenerator):

    def __init__(self, config_object, maximum):
        super(EntityGenerator, self).__init__(config_object, maximum)
        self.fake = faker.Faker()
        self.eid = -1

    def _get_name(self):
        return self.fake.name()

    def _get_id(self):
        self.eid += 1
        return self.eid
