import random
import string

from tts_gen.generators import base


class LocationGenerator(base.BaseGenerator):

    def __init__(self, config_object, maximum):
        super(LocationGenerator, self).__init__(config_object, maximum)
        self.eid = -1

    def _get_name(self):
        nb = random.randint(1, 999)
        suffix = random.choice(string.ascii_uppercase)
        return f"{nb}{suffix}"

    def _get_id(self):
        self.eid += 1
        return self.eid
