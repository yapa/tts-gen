import random

from tts_gen.generators import base


class EventGenerator(base.BaseGenerator):

    def __init__(self, config_object, maximum):
        super(EventGenerator, self).__init__(config_object, maximum)
        self.eid = -1

    def _get_name(self):
        return random.choice(self.config.events)

    def _get_id(self):
        self.eid += 1
        return self.eid
