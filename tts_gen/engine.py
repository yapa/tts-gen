from tts_gen.utils.constraint import ConstraintBuilder
from tts_gen.utils.feature import FeatureBuilder
from tts_gen import generators as gen


def get_result(locations, events, entities, ts_in_tc, cfg):

    ts = ts_in_tc * len(cfg.ts_groups)
    result = {
        "locations":
            [x for x in gen.location.LocationGenerator(cfg, locations)],
        "entities":
            [x for x in gen.entity.EntityGenerator(cfg, entities)],
        "events":
            [x for x in gen.event.EventGenerator(cfg, events)],
        "time_slots":
            [x for x in gen.time_slot.TimeSlotGenerator(cfg, ts)]
    }

    constraint_builder = ConstraintBuilder()
    for key in result.keys():
        for val in result[key]:
            val["constraints"] = constraint_builder.create_constraints(
                    key, val["name"])

    feature_builder = FeatureBuilder(
            constraint_builder.get_required_features())
    for key in result.keys():
        for val in result[key]:
            val["features"] = feature_builder.create_features(key, val["name"])
    return result
