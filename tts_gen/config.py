import yaml


class Config(object):

    def __init__(self, file_name, **kwargs):
        with open(file_name, 'r') as f:
            self.field_names = yaml.load(f)

        self.kwargs = kwargs

    @property
    def skills(self):
        return self.field_names["SKILLS"]

    @property
    def location_types(self):
        return self.field_names["LOCATION_TYPES"]

    @property
    def ts_groups(self):
        return self.field_names["TS_GROUPS"]

    @property
    def events(self):
        return self.field_names["EVENTS"]

    def get_global_variable(self, name):
        if name not in self.kwargs:
            self.kwargs[name] = -1
        return self.kwargs[name]

    def set_global_variable(self, name, value):
        self.kwargs[name] = value
