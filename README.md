Utility for generating timetable scheduling problem instances 

1. Create virtualenv: `/user/bin/python3.6 -m venv some_vevn`
2. Launch virtualenv: `source venv/bin/activate`
3. Install requirements: `pip install -r requirements.txt`
4. Basic run: `python main.py --help`



Notes:
1. $`\#TS * \#LOCATIONS = max(EVENTS)`$
2. \#LOCATIONS depends on \#ENTITIES (2 loc -> 2 possible EVENTS in one TS -> 2 ENTITIES required)
